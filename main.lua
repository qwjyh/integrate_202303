---@module 'lib'
local lib = require'lib'

---Try integration with `n` steps and print result.
---@param n integer
---@return nil
local function try_int_withnstep(n)
    ---@type Range
    local int_range = { start = -2, stop = 2 }

    print(n .. '\t' .. lib.integrate(lib.f, n, int_range))
end

for i=1,25 do
    try_int_withnstep(10 * 2 ^ i)
end

