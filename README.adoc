= Calc integral
qwjyh <qwjyh48@gmail.com>
v1.0, 2023-03-15
:toc:
:stem: latexmath
:source-highlighter: highlightjs

== What's this.
Numerically calculate integral 
[stem]
++++
\int_{-2}^{2} \left( x^3 \cos \frac{x}{2} + \frac{1}{2} \right) \sqrt{4 - x^2} d x
++++

== How to execute.
Run `main.lua` with lua or luajit.

== Generate docs
.README.html
[source, sh]
----
$ asciidoc README.adoc -o docs/README.html
----

.doc.md
Use `lua-language-server`.
(I used vscode-extension feature.)
