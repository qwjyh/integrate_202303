---@class Lib
local M = {}

---Integrated function
---@param x number
---@return number
M.f = function(x)
    return (x^3 * math.cos(x / 2) + 1 / 2) * math.sqrt(4 - x^2)
end

---@class Range
---@field start number
---@field stop number

---Integrate function `f` with `n` steps for range `ran`
---@param f fun(x: number): number
---@param n integer
---@param ran Range
---@return number
local function integrate(f, n, ran)
    local dx = (ran.stop - ran.start) / n
    ---@type number
    local ans = 0
    for i = 0, n-1 do
        local x = ran.start + dx * i
        ans = ans + f(x) * dx
    end
    return ans
end

M.integrate = integrate

return M
